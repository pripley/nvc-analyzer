from django.core.management.base import BaseCommand
from nvcapp.models import Feelings, Needs, ObservationPreambles

class Command(BaseCommand):
    args = '<foo bar ...>'
    help = 'our help string comes here'

    def get_file_options(self, filename):
        option_list = []
        f = open(filename, 'r')
        for line in f:
            option_list.append(line.rstrip("\n"))
        f.close()
        return option_list

    def get_needs(self):
        return self.get_file_options("needs_list.txt")

    def get_feelings(self):
        return self.get_file_options("feelings_list.txt")

    def _create_observation_preambles(self):
        tlisp = ObservationPreambles(observation_preamble='When I see')
        tlisp.save()
        tlisp = ObservationPreambles(observation_preamble='When I hear')
        tlisp.save()
        tlisp = ObservationPreambles(observation_preamble='When I hear you say')
        tlisp.save()

    def _create_needs(self):
        for n in self.get_needs():
            tlisp = Needs(need=n)
            tlisp.save()

    def _create_feelings(self):
        for f in self.get_feelings():
            tlisp = Feelings(feeling=f)
            tlisp.save()


    def handle(self, *args, **options):
        #self._create_observation_preambles()
        #self._create_feelings()
        self._create_needs()
