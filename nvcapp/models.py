from django.db import models
from django import forms
# Create your models here.
from django.db import models

class ObservationPreambles(models.Model):
    observation_preamble = models.CharField(max_length=30)

    def __str__(self):
        return self.observation_preamble

class Feelings(models.Model):
    feeling = models.CharField(max_length=30)

    def __str__(self):
        return self.feeling.lower()

class Needs(models.Model):
    need = models.CharField(max_length=30)

    def __str__(self):
        return self.need.lower()
    
class NVCText(models.Model):
    observation_selected = models.ForeignKey('ObservationPreambles')
    observation_text = models.CharField(max_length=100, help_text='e.g. leave the lights on all day',)
    feelings_selected = models.ManyToManyField('Feelings')
    needs_selected = models.ManyToManyField('Needs')
    request_text = models.CharField(max_length=100, help_text='e.g., share your perspective on the issue?',)

    def publish(self):
        self.save()
