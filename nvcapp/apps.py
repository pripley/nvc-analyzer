from django.apps import AppConfig


class NvcappConfig(AppConfig):
    name = 'nvcapp'
