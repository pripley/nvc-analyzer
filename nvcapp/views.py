from django.shortcuts import render
from django.conf import settings
from nvc_natural_language_support import * 
from .forms import NVCForm
from .models import Feelings, Needs, ObservationPreambles

def convert_multi_selections_to_string(items):
    print("items:",items)

    num_elements = len(items)
    if num_elements == 0:
        return ""
    if num_elements == 1:
        return items[0]
    if num_elements == 2:
        return items[0] + " and " + items[1]
    else:
        str = ""
        for i in items[:num_elements-1]:
            str += i + ", "
        str += "and " + items[num_elements-1]
        return str            
    return ""


def get_file_options(filename):
    option_list = []
    f = open(filename, 'r')
    for line in f:
        option_list.append(line.rstrip("\n").lower())
    f.close()
    return option_list

def get_needs():
    return get_file_options("needs_list.txt")

def get_feelings():
    return get_file_options("feelings_list.txt")


# Main landing page	
def index(request):
    if request.method == 'GET':
        form = NVCForm(initial={'observation_selected': 1})
        print("form:", form)
        return render(request, 'index.html', {'form': form})
##        feelings_list = get_feelings()
##        needs_list = get_needs()
##        return render(request, 'index.html', {'needs_list': needs_list, 'feelings_list': feelings_list})
    elif request.method == 'POST':
        form = NVCForm(request.POST)
##        if form.is_valid():
##            nvcf = form.save(commit=False)
##            observation_selected = nvcf.observation_selected
##            observation_text = nvcf.observation_text
##            feelings_selected = nvcf.feelings_selected
##            print(feelings_selected)
##            feelings_text = convert_multi_selections_to_string(feelings_selected)
##            needs_selected = nvcf.needs_selected
##            needs_text = convert_multi_selections_to_string(needs_selected)
##            request_text = nvcf.request_text
        observation_selected = request.POST.get('observation_selected', '')
        observation_selected = ObservationPreambles.objects.get(id=observation_selected).observation_preamble
        observation_text = request.POST.get('observation_text', '')
        if observation_selected == "When I hear you say":
            observation_text = '"' + observation_text + '"'

        feelings_selected = request.POST.getlist('feelings_selected', '')
        feelings_selected = list(map(lambda x: Feelings.objects.get(id=x).feeling, feelings_selected))
        print(feelings_selected)
        feelings_text = convert_multi_selections_to_string(feelings_selected)
        needs_selected = request.POST.getlist('needs_selected', '')
        needs_selected = list(map(lambda x: Needs.objects.get(id=x).need, needs_selected))

        needs_text = convert_multi_selections_to_string(needs_selected)
        request_text = request.POST.get('request_text', '')
        resulting_text = observation_selected + " " + observation_text + ", " + "I feel " + feelings_text + ".  I have need(s) for " + needs_text + ".  Would you be willing to " + request_text

        #Perform sentiment analysis on the text fields
        nvc = nvc_language_analyzer()
        observation_results = nvc.apply_nvc_analysis([observation_text], True, False)
        request_results = nvc.apply_nvc_analysis([request_text], True, True)
        #print("observation r:",observation_results)
        #print("request r:",request_results)
        analysis_text = observation_results + request_results

##        analysis_text = ""
##        if observation_results != None:
##            for r in observation_results:
##                analysis_text += '"' + r[0] + '"'
##                for t in r[1]:
##                    analysis_text += '"' + t[0] + '": ' + t[1] + '\n'
##        if request_results != None:
##            for r in request_results:
##                analysis_text += '"' + r[0] + '"'
##                for t in r[1]:
####                    print("t[0]:", t[0])
####                    print("t[1]:", t[1])
##                    analysis_text += '"' + t[0] + '": ' + t[1] + '\n'
##        print("analysis text:", analysis_text)    
        return render(request, 'index.html', {'form': form,'flashmessage': analysis_text, 'resulting_text': resulting_text})
