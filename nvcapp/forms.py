from django import forms
from django.forms import TextInput
from .models import NVCText

class NVCForm(forms.ModelForm):
    class Meta:
        model = NVCText
        fields = ('observation_selected', 'observation_text',
                  'feelings_selected', 'needs_selected', 'request_text')
        widgets = {
            'observation_text': TextInput(attrs={'placeholder': 'A factual and non-evaluative observation', 'cols':450}),
            'request_text': TextInput(attrs={'placeholder': 'A clear, specific, and doable action', 'width':450}),
        }
##        placeholders = {
##            'request_text': ('A clear, specific, and doable action'),
##            'observation_text': ('A factual and non-evaluative observation'),
##        }
        labels = {
            'observation_selected': ('Observation'),
            'observation_text': (''),
            'feelings_selected': ('I feel'),
            'needs_selected': ('I need'),
            'request_text': ('Would you be willing to'),
        }
