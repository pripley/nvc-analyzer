This project's aim is to use principles of Non-violent communication (NVC) to assist individuals with difficult conversations.

The project makes use of Python's nltk (natural language toolkit) to determine the sentiment of text to detect language that may not be helpful when making observations about or requests of others.  

Examples

1. "Show more respect" - is vague; a more concrete / specific request would be better e.g., would you be willing to show up on time for our meetings? 
2. "Would you be willing to stop interrupting?" - As per NVC principles, requests should be what we want rather than what we don't want
3. "When I see you belittling Suzy" - the word "belittling" has an evaluation built into it; instead, observations should be neutral.  e.g., when I hear you calling Suzy "a jerk"


In addition to a django web front end, a web service is included for potential future app development.