import nltk
from nltk.corpus import state_union
from nltk.corpus import sentiwordnet as swn
from nltk.corpus.reader.wordnet import WordNetError
from nltk.tokenize import PunktSentenceTokenizer
from nltk.corpus import opinion_lexicon
from nltk.corpus import subjectivity
from nltk.corpus import sentence_polarity
from nltk.sentiment.vader_pr import SentimentIntensityAnalyzer
from nltk.stem import *
from nltk.stem.porter import *

negative_word_list = []

NEGATIVE_WORD = "1"
POSITIVE_WORD = "2"
NEGATIVE_REQUEST = "3"

NEGATE = \
["aint", "arent", "cannot", "cant", "couldnt", "darent", "didnt", "doesnt",
 "ain't", "aren't", "can't", "couldn't", "daren't", "didn't", "doesn't",
 "dont", "hadnt", "hasnt", "havent", "isnt", "mightnt", "mustnt", "neither",
 "don't", "hadn't", "hasn't", "haven't", "isn't", "mightn't", "mustn't",
 "neednt", "needn't", "never", "none", "nope", "nor", "not", "nothing", "nowhere",
 "oughtnt", "shant", "shouldnt", "uhuh", "wasnt", "werent",
 "oughtn't", "shan't", "shouldn't", "uh-uh", "wasn't", "weren't",
 "without", "wont", "wouldnt", "won't", "wouldn't", "rarely", "seldom", "despite", "n't"]

NEG_REQUEST_WORDS = \
                  ["stop", "disist", "quit", "not", "discontinue",
                   "cease", "curb", "halt", "end"]

VAGUE_LANGUAGE_MARKERS = \
               ["more","less" ]


WORDS_TO_IGNORE = \
                ["want", "like", "time", "live", "living", "lives", "work", "issue", "pass", "passes", "passed",
                 "hospital", "hospitals"]

OBSERVATION_EXAGGERATION_WORDS = \
                          ["never", "always"]

OBSERVATION_EXAGGERATION_PHASES = \
                                ["all the time",]
ADDITIONAL_NEGATIVE_WORDS = \
                          ["asshole", "drama"]
NEGATIVE_IDIOMS = \
                ["behind my back", "drama queen", "steal my thunder", "wild goose chase", "screw the pooch",
                 "jump ship", "cutting the mustard", "clam up", "at the drop of a hat",
                 "taken aback", "across the board", "devil's advocate", "acting your age", "act your age",
                 "beating around the bush", "rocking the boat", "missing the boat", "burning your bridges",
                 "caving in", "cross purposes", "cutting me off", "raw deal", "going off the deep end",
                 "digging yourself into a hole", "doubting Thomas", "running on empty",
                 "closing your eyes to", "turn a blind eye", "turning a blind eye", "getting in my face",
                 "losing face", "without fail", "fall on deaf ears", "taking the fall", "take the fall",
                 "fed up", "flat-footed", "flying in the face of", "one foot in the grave", "putting your foot down",
                 "on the fritz", "go bananas", "going bananas", "go postal", "going postal", "lower your guard",
                 "taking a hammering", "take a hammering", "sitting on your hands", "washing your hands of",
                 "hanging me out to dry", "hanging over your head", "lose your head", "head in the clouds",
                 "digging in your heels", "dragging your feet", "dragging your heels", "hot under the collar",
                 "having it in for", "running interference", "jumping the queue", "jumping on the bandwagon",
                 "jumping to conclusions", "kicking butt", "kicking ass", "kicking up a fuss", "kicking up a stink",
                 "resting on your laurels", "taking the law into your own hands", "on your last legs",
                 "letting off steam","letting yourself go", "taking liberties", "pay lip service",
                 "looking down on", "looking the other way", "a losing battle", "losing sleep",
                 "getting the message", "burning the midnight oil", "making a mountain out of a molehill",
                 "making a monkey"]

def is_negation_present(words): 
    for w in words:
        x = nltk.word_tokenize(w)
        print(x)
        for y in x:
            print(y)
            if y in NEGATE:
                return True
    return False

def assess_phrase_parts_of_speech(sentence):
    #get the part of speech
    train_text = state_union.raw("2005-GWBush.txt")
    #sample_text = state_union.raw("2006-GWBush.txt")
    custom_sent_tokenizer = PunktSentenceTokenizer(train_text)
    return custom_sent_tokenizer.tokenize(sentence)

def convert_Punkt_pos_to_sentiwordnet_pos(pos):
    pos_1st_letter = pos[0:1].lower()
    if pos_1st_letter == "j":
        return "a"
    else:
        return pos_1st_letter
        

    

class nvc_language_analyzer:
    m_stemmer = None

    def __init__(self):
        self.negative_word_list = []
        self.positive_word_list = []

        #get the part of speech
        train_text = state_union.raw("2005-GWBush.txt")
        self.custom_sent_tokenizer = PunktSentenceTokenizer(train_text)

        self.m_stemmer = PorterStemmer()
        g = opinion_lexicon.raw("negative-words.txt")
        k = list(g.split("\n"))
        for w in k:
            if w[0:1] != ";" and w[0:1] != "":
                self.negative_word_list.append(self.m_stemmer.stem(w))
        add_neg_words = list(map(lambda x: self.m_stemmer.stem(x), ADDITIONAL_NEGATIVE_WORDS))
        self.negative_word_list = self.negative_word_list + add_neg_words
        self.negative_word_list = set(self.negative_word_list)

        g = opinion_lexicon.raw("positive-words.txt")
        k = list(g.split("\n"))
        for w in k:
            if w[0:1] != ";" and w[0:1] != "":
                self.positive_word_list.append(self.m_stemmer.stem(w))
        self.positive_word_list = set(self.positive_word_list)
        

    def filter_out_quotes(self, sentence):
        r = ""
        is_in_quote = False

        for s in sentence:
            if s == '"':
            #if s == '"' or s == "'":
                if is_in_quote == False:
                    is_in_quote = True
                else:
                    is_in_quote = False
            else:
                if is_in_quote == False:
                    r += s
        return r

    def check_for_idioms(self, sentence):
        r = []
        for i in NEGATIVE_IDIOMS:
            if i in sentence:
                r.append((i, "'" + i + "' appears to be an idiom - observations/requests should have a neutral sentiment and be free from vagueness"))
                print("idiom:", r)
        return r
                
##    def filter_out_words_to_ignore(self, tokenized):
##        r = []
##        for s in tokenized:
##            if s not in WORDS_TO_IGNORE:
##                r.append(s)

    def assess_word_sentiment(self, word, pos):

        #determine the pos / neg of the word (take the first definition we find)
        try:
            

            sentiment_assessed_word = swn.senti_synset(word + "." + pos + ".01")
            #sentiment_assessed_word = list(swn.senti_synset(word, pos))
            print("word: {0} pos:{1} positive:{2} negative:{3}".format(word, pos, sentiment_assessed_word.pos_score(), sentiment_assessed_word.neg_score()))
        except WordNetError:
            return 0,0
        return sentiment_assessed_word.pos_score(), sentiment_assessed_word.neg_score()


    def find_neg_word(self, s):
        result = []
        for x in self.negative_word_list:
            for i in s:
                words = nltk.word_tokenize(i)
                for w in words:
                    if w not in WORDS_TO_IGNORE:
                        y = self.m_stemmer.stem(w)
                        if x == y:
                            result.append((w, "'" + w + "' has a negative conntation - observations/requests should have a neutral sentiment and be free from vagueness"))
        return result
    def find_pos_word(self, s):
        result = []
        for x in self.positive_word_list:
            for i in s:
                words = nltk.word_tokenize(i)
                #print("pos words:", words)
                for w in words:
                    if w not in WORDS_TO_IGNORE:
                        y = self.m_stemmer.stem(w)
                        if x == y:
                            result.append((w, "'" + w + "' has a positive conntation - observations/requests should have a neutral sentiment and be free from vagueness"))
        return result

    def assess_sentence_sentiment(self, tokenized):
        total_neg_score = 0
        total_pos_score = 0
        results = []
        try:
            for i in tokenized:
                words = nltk.word_tokenize(i)
                tagged = nltk.pos_tag(words)
                print(tagged)
                #loop through each word in the sentence
                for j in tagged:
                    pos_score, neg_score = self.assess_word_sentiment(j[0], convert_Punkt_pos_to_sentiwordnet_pos(j[1]))
                    if pos_score > 0 or neg_score > 0: results.append((j[0], "Non neutral word"))
                    total_neg_score += neg_score
                    total_pos_score += pos_score
    ##                print("word:", j[0])
    ##                #print("pos:", j[1])
    ##                #print("posmapped:", convert_Punkt_pos_to_sentiwordnet_pos(j[1]))
    ##                print("neg:", neg_score)
    ##                print("pos:", pos_score)
                return results
                #return total_pos_score, total_neg_score
                                                                 

        except Exception as e:
            print(str(e))
        return 0,0

    #check for a negative request word if is 1st verb or if it precedes the 1st verb
    #negative request words maybe okay in later parts of the sentence
    def is_negative_request(self, tokenized):
        for i in tokenized:
            words = nltk.word_tokenize(i)
            tagged = nltk.pos_tag(words)
            #print(tagged)
            verb_seen = False
            #loop through each word in the sentence
            index_j = 1
            length_j = len(tagged)

            for j in tagged:
                #print(j)
                if j[0] in NEG_REQUEST_WORDS and verb_seen == False:
                    #return True
                    return (j[0], "'"+ j[0] +"' - consider a request that says what you want rather than what you don't want")
                if convert_Punkt_pos_to_sentiwordnet_pos(j[1]) == "v":
                    verb_seen = True
##                    print("verb seen:", tagged[index_j][0])
##
##                    if index_j < length_j and tagged[index_j][0] in NEG_REQUEST_WORDS:
##                        return (j[0], "'"+ j[0] +" " + tagged[index_j][0] + "' - consider a request that says what you want rather than what you don't want")
##                        
##                index_j = index_j + 1
        return None

    # test for observations starting with "never" or "always" or that contain "all the time"
    def test_obs_exaggerations(self, sentence):
        s = sentence.split()
        if len(s) > 0:
            if s[0] in OBSERVATION_EXAGGERATION_WORDS:
                return (s[0], "observational exaggeration - consider dropping '" + s[0] + "' from the observation")
        for egp in OBSERVATION_EXAGGERATION_PHASES:
            if egp in sentence:
                return (egp, "observational exaggeration - consider dropping '" + egp + "' from the observation")
        

    def test_req_vagueness(self, tokenized):
        for i in tokenized:
            words = nltk.word_tokenize(i)
            tagged = nltk.pos_tag(words)
            
            #loop through each word in the sentence
            index_t = 1
            length_t = len(tagged)
            for j in tagged:
                if j[0] in VAGUE_LANGUAGE_MARKERS and index_t < length_t:
                    k = convert_Punkt_pos_to_sentiwordnet_pos(tagged[index_t][1])
                    if k == "a" or k == "r":
                        return (j[0], "your request is potentially vague - the use of '" + j[0] + " " + tagged[index_t][0] + "' in this context may be vague.")
                index_t = index_t + 1
        return None        


    def apply_nvc_analysis(self, sentences, printresults = False, is_request = False):
        all_results = []
        sid = SentimentIntensityAnalyzer()

        
        #sentences = list(map(lambda x: self.filter_out_quotes(x), sentences))

        for s in sentences:
            #filter out quoted text (only ") since quoted text should affect sentiment
            s_nq = self.filter_out_quotes(s)

            s_results = []
            if printresults: print(s)
            tokenized_words = self.custom_sent_tokenizer.tokenize(s_nq)

            #vader analysis
            
            #print(sentence)
##            ss = sid.polarity_scores(s_nq)
##            if ss["neu"] != 1:
##                s_results.append(("Non-neutral sentiment", "Observations / requests should be free from evaluative language"))
            s_results = s_results + self.check_for_idioms(s)

            if is_request == True:
                s_results.append(self.is_negative_request(tokenized_words))
                s_results.append(self.test_req_vagueness(tokenized_words))

            else:
                # must be an observation
                s_results.append(self.test_obs_exaggerations(s))


            s_results = s_results + self.find_neg_word(tokenized_words)
            s_results = s_results + self.find_pos_word(tokenized_words)
##            k = self.assess_sentence_sentiment(tokenized_words)
            

            #s_results.append(self.assess_sentence_sentiment(tokenized_words))
            s_results = [i for i in s_results if i is not None]
            #if printresults: print("results:",s_results)
            all_results.append((s, s_results))
        return all_results
##            print("pos score:",pos_score)
##            print("neg score:",neg_score)
##            if pos_score == 0 and neg_score == 0:
##                if printresults: print("neutral sentiment")
##            else:
##                if pos_score > neg_score:
##                    if printresults: print("positive sentiment")
##                else:
##                    if printresults: print("negative sentiment")
##            if printresults: print("\n")

    def analyze_sentence(self, s, is_request = False):
        #filter out quoted text (only ") since quoted text should affect sentiment
        s_nq = self.filter_out_quotes(s)

        s_results = []
        tokenized_words = self.custom_sent_tokenizer.tokenize(s_nq)

        if is_request == True:
            s_results.append(self.is_negative_request(tokenized_words))
            s_results.append(self.test_req_vagueness(tokenized_words))
        else:
            # must be an observation
            s_results.append(self.test_obs_exaggerations(s))

        s_results = s_results + self.check_for_idioms(s)
        s_results = s_results + self.find_neg_word(tokenized_words)
        s_results = s_results + self.find_pos_word(tokenized_words)
        s_results = [i for i in s_results if i is not None]
        return s_results

if __name__ == '__main__':



    sentences = ["VADER is smart, handsome, and funny.",
                 "You never put the remote where it belongs.",
                 "The dishes were still in the dishwasher when I came home.",
                 "You missed two appointments last month.",
                 "She has terrible interpersonal skills.",
                 "You're always asking me to clean up after your messes.",
                 "The president didn't ask for the year end report during the meeting.",
                 "People complained about the lack of professionalism.",
                 "He spoke on the cell phone during the procedure.",
                 "The Director gets angry over the smallest things.",
                 "The receptionist said, 'You are being very difficult, sir.'",
                 "Twelve percent of the student were slow learners.",
                 "The supervisor praised the team generously.",
                 "The new employee was not as productive as other employees.",
                 'that you want to "fuck the police"',]
    sentences2 = ["Would you be willing to give me 24 hours notice if you can't attend?",
                  "Would you be willing to be more sensitive?",
                  "Don't speak to me in that tone",
                  "Would you be willing to tell me what you're hearing me say?",
                  "It would be ideal if you could get this to me by noon.",
                  "Is there more you would like me to hear?",
                  "Would you be willing to fill in for me tomorrow?",
                  "Can you tell me how you're feeling?",
                  "I would like you to show more respect please.",
                  "Would you be willing to share your perspective on this issue?",
                  "not speak to me in that tone?"]
    sentences3 = ["I want you to understand me.",
                  "I'd like you to tell me one thing that I did that you appreciate.",
                  "I'd like you to feel more confidence in yourself.",
                  "I want you to stop drinking.",
                  "I'd like you to let me be me.",
                  "I'd like you to be honest with me about yesterday's meeting.",
                  "I would like you to drive at or below the speed limit.",
                  "I'd like to get to know you better",
                  "I would like you to show respect for my privacy",
                  "I'd like you to prepare supper more often"]
    sentences4 = ["You are too generous.",
                  "Doug procrastinates.",
                  "When I see you give all your lunch money to others, I think you are being too generous",
                  "She won't get her work in.",
                  "I don't think she'll get her work in.",
                  "She said, 'I won't get my work in.'",
                  "If you don't eat balanced meals, your health will be impaired.",
                  "If you don't eat balanced meals, I fear your health may be impaired.",
                  "Immigrants don't take care of their property.",
                  "I have not seen the immigrant family living at 1679 Ross shovel the snow on their sidewalk.",
                  "Hank Smith is a poor soccer player.",
                  "Hank Smith has not scored a goal in twenty games.",
                  "Jim is ugly.",
                  "Jim's looks don't appeal to me.",
                  "Whenever I have observed Jack on the phone, he has spoken for at least thirty minutes.",
                  "I cannot recall you ever writing to me.",
                  "You are always busy.",
                  "She is never there when she's needed.",
                  "When I see you acting like an asshole",
                  "always fucking up the dishes",
                  "messing up the dishes all the time",
                  "Johnny is the shit",]
    nvcs = nvc_language_analyzer()
    #print("filter test:", nvcs.filter_out_quotes("Mary had a 'little' lamb"))
    #print("filter test:", nvcs.filter_out_quotes('Jack and Jill went "up" the hill'))
    #sentences_minus_quoted_text = list(map(lambda x: nvcs.filter_out_quotes(x), sentences))
    #print("no quotes",sentences_minus_quoted_text)

    results = nvcs.apply_nvc_analysis(sentences4, True, False)
    print("Results:")
    for r in results:
        print("\n", r[0])
        print(r[1])
    
##    sid = SentimentIntensityAnalyzer()
##    for sentence in sentences_minus_quoted_text:
##        print(sentence)
##        ss = sid.polarity_scores(sentence)
##        for k in sorted(ss):
##            print('{0}: {1}, '.format(k, ss[k]), end='')
##            print()
    print("done")



