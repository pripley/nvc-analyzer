#!/usr/bin/env python
import bottle
from bottle import route, run, template, get, post, request, response
from nvc_natural_language_support import * 
import json


app = application = bottle.default_app()
nvc = nvc_language_analyzer()


@get('/evalobservation/<obs>')
def obs_handler(obs):
    observation_results = nvc.analyze_sentence(obs, False)
    output = '{"success": true, "results": ['
    for r in observation_results:
        output += '{"msg": "'+ r + '"},'
    output += ']}'
    print(output)
    response.headers['Content-Type'] = 'application/json'
    response.headers['Cache-Control'] = 'no-cache'
    return json.dumps(output)

#@post('/evalnvcsents')
@get('/evallnvcsents')
def nvc_form_handler():
    '''Handles observations and requests from nvc form'''

##    try:
##        # parse input data
##        try:
##            data = request.json()
##        except:
##            raise ValueError
##
##        if data is None:
##            raise ValueError
##
##        # extract and validate name
##        try:
##            if namepattern.match(data['observation_text']) is None or namepattern.match(data['request_text']) is None:
##                raise ValueError
##            observation_text = data['observation_text']
##            request_text = data['request_text']
##
##        except (TypeError, KeyError):
##            raise ValueError
##
##    except ValueError:
##        # if bad request data, return 400 Bad Request
##        response.status = 400
##        return
    observation_text = "being a jerk"
    request_text = "stop being a jerk"
    observation_results = nvc.analyze_sentence(observation_text, False)        
    request_results = nvc.analyze_sentence(request_text, True)
    output = '{"success": true, "sentences": ['
    output += '{"sentence":'+ observation_text +', "results": [' 
    for r in observation_results:
        output += '{"msg": "'+ r[1] + '"},'
    output += ']}'
    output += '{"sentence":'+ request_text +', "results": [' 
    for r in request_results:
        output += '{"msg": "'+ r[1] + '"},'
    output += ']}'
    print(output)

    response.headers['Content-Type'] = 'application/json'
    return json.dumps(output)

@get('/evalrequest/<req>')
def req_handler(req):
    request_results = nvc.analyze_sentence(req, True)
    output = '{"success": true, "results": ['
    for r in request_results:
        output += '{"msg": "'+ r[1] + '"},'
    output += ']}'
    print(output)

    response.headers['Content-Type'] = 'application/json'
    response.headers['Cache-Control'] = 'no-cache'
    return json.dumps(output)

if __name__ == '__main__':
    bottle.run(host = '127.0.0.1', port = 8000)
